package com.supermap.services.security.ext.gzca;

/*
 * {
    "safetyClass": "2",
    "name": null,
    "realName": "谷永权",
    "idCard": "522321198702037616",
    "createTime": "2020-05-07T07:12:39.000+0000",
    "orgCodes": null,
    "uid": "988540599bd28adc81a6fab8a135261e2363e57d5b2ac3064850ad83f5f65ab6"
}
 */
public class PostUserResult {
    public String safetyClass;
    public String realName;
    public String idCard;
    public String uid;
}
