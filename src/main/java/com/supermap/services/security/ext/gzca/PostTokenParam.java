package com.supermap.services.security.ext.gzca;

//{\"authCode\":\"%s\",\"grantType\":\"authorization_code\",\"platCode\":\"%s\",\"sign\":\"%s\"}", authCode, platCode, sign)
public class PostTokenParam {
    public String authCode;
    public String grantType = "authorization_code";
    public String platCode;
    public String sign;

}
