package com.supermap.services.security.ext.gzca;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

public class AtomicInitializer<T> {
    private final AtomicReference<T> reference = new AtomicReference<>();
    public T get(Supplier<T> initialize)  {
        T result = reference.get();

        if (result == null) {
            result = initialize.get();
            if (!reference.compareAndSet(null, result)) {
                // another thread has initialized the reference
                result = reference.get();
            }
        }

        return result;
    }

}
