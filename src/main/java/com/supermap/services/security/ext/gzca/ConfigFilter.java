package com.supermap.services.security.ext.gzca;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import com.google.common.net.MediaType;
/*
 * 
 * 拦截 /iportal/resources/web-ui/extend/login/config.json 请求输出期望配置以便让iportal使用自定义页面登录。
 *
 */
public class ConfigFilter implements Filter {

    private String content = "{\"enable\":true,\"extendType\":\"IFRAME\",\"loginUrl\":\"{portalRoot}/gzca/\",\"registerUrl\":\"{portalRoot}/resources/web-ui/extend/login/register.html\",\"resetPwdUrl\":\"{portalRoot}/resources/web-ui/extend/login/resetPwd.html\"}";
    
    public void setContent(String value) {
        content = value;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // TODO Auto-generated method stub

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse res = (HttpServletResponse) response;
        res.setCharacterEncoding(StandardCharsets.UTF_8.name());
        res.setContentType(MediaType.JSON_UTF_8.toString());
        try(PrintWriter writer = res.getWriter()) {
            writer.append(content);
        }
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub

    }
}
